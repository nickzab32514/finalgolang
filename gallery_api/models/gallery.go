package models

import (
	"fmt"
	"os"
	"path/filepath"
	"strconv"

	"github.com/jinzhu/gorm"
)

//สร้างมาเพื่อแยกการทำงานทีซับซ้อนของ db
type Gallery struct {
	gorm.Model
	Name      string
	IsPublish bool
	UserID    uint
	Images    []Image
}

// var _ GalleryService = &galleryGorm{}

//ตัวที่handlerใช้ในการรับรอง
type GalleryService interface {
	Create(gallery *Gallery) error
	ListGalleryUser(id uint) ([]Gallery, error)
	Delete(id uint) error
	GetByID(id uint) (*Gallery, error)
	UpdateGallery(gallery *Gallery) error
	UpdateGalleryPublishing(id uint, isPublish bool) error
	ListAllPublish() ([]Gallery, error)
}

type galleryGorm struct {
	// gs models.GalleryService
	db *gorm.DB
}

func NewGalleryService(db *gorm.DB) GalleryService {
	return &galleryGorm{db}
}
func (gg *galleryGorm) ListGalleryUser(id uint) ([]Gallery, error) {
	gallerys := []Gallery{}
	if err := gg.db.Where("user_id = ?", id).Find(&gallerys).Error; err != nil {
		return nil, err
	}
	return gallerys, nil
}

func (gg *galleryGorm) ListAllPublish() ([]Gallery, error) {
	galleries := []Gallery{}
	err := gg.db.
		Where("is_publish = ?", true).
		Find(&galleries).Error
	if err != nil {
		return nil, err
	}
	for i := 0; i < len(galleries); i++ {
		images := []Image{}
		err := gg.db.
			Where("gallery_id = ?", galleries[i].ID).
			Find(&images).Error
		if err != nil {
			return nil, err
		}
		galleries[i].Images = images
	}
	return galleries, nil
}

func (gg *galleryGorm) Create(gallery *Gallery) error {
	fmt.Println("test: ", gallery)
	return gg.db.Create(gallery).Error
}
func (gg *galleryGorm) Delete(id uint) error {
	bg := gg.db.Begin()
	defer func() {
		if r := recover(); r != nil {
			bg.Rollback()
		}
	}()
	if err := gg.db.Where("id = ?", id).Delete(Gallery{}).Error; err != nil {
		bg.Rollback()
		return err
	}
	if err := gg.db.Where("gallery_id = ?", id).Delete(&Image{}).Error; err != nil {
		bg.Rollback()
		return err
	}
	idStr := strconv.FormatUint(uint64(id), 10)
	if err := os.RemoveAll(filepath.Join(UploadPath, idStr)); err != nil {
		fmt.Println("Fail deleting image file ==", err)
		return err
	}

	return bg.Commit().Error

}
func (gg *galleryGorm) GetByID(id uint) (*Gallery, error) {
	tt := new(Gallery)
	fmt.Println(id)
	if err := gg.db.First(tt, id).Error; err != nil {
		return nil, err
	}
	fmt.Println("id====>", id)
	fmt.Println("tt====>", tt)

	return tt, nil
}
func (gg *galleryGorm) UpdateGallery(gallery *Gallery) error {
	if err := gg.db.Model(gallery).Where("id = ?", gallery.ID).Update("name", gallery.Name).Error; err != nil {
		// if err := gg.db.Model(gallery).Update(gallery).Error; err != nil {
		return err
	}
	return nil
}
func (gg *galleryGorm) UpdateGalleryPublishing(id uint, isPublish bool) error {
	return gg.db.Model(&Gallery{}).Where("id = ?", id).Update("is_publish", isPublish).Error
}
