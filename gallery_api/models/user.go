package models

import (
	"fmt"
	"gallery-api/hash"
	"gallery-api/rand"

	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/bcrypt"
)

type User struct {
	gorm.Model
	Email    string `gorm:"unique_index;not null"`
	Password string `gorm:"not null"`
	Username string `gorm:"not null"`
	Token    string `gorm:"unique_index"`
}
type userGorm struct {
	db   *gorm.DB
	hmac *hash.HMAC
}

var hmacKey = "galgall"

func NewUserService(db *gorm.DB, key string) UserService {
	hmac := hash.NewHMAC(key)
	return &userGorm{db, hmac}

}

type UserService interface {
	SignupUser(user *User) error
	LoginUser(user *User) (string, error)
	GetByToken(token string) (*User, error)
	LogoutUser(user *User) error
}

func (ug *userGorm) SignupUser(tmp *User) error {
	user := new(User)
	user.Email = tmp.Email
	user.Password = tmp.Password
	hash, err := bcrypt.GenerateFromPassword([]byte(user.Password), 12)
	if err != nil {
		return err
	}
	user.Password = string(hash)
	token, err := rand.GetToken()
	if err != nil {
		return err
	}
	tokenHash := ug.hmac.Hash(token)

	user.Token = tokenHash
	tmp.Token = token
	return ug.db.Create(user).Error

}
func (ug *userGorm) LoginUser(user *User) (string, error) {
	fmt.Println("=======================================called")
	dbUser := new(User)
	err := ug.db.Where("Email = ?", user.Email).First(&dbUser).Error
	if err != nil {
		return "1", err
	}
	fmt.Println("from req got===>", user.Password)
	fmt.Println("from db got===>", dbUser.Password)
	bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(dbUser.Password))
	if err != nil {
		fmt.Println("ERR", err)
		return "2", err
	}

	token, err := rand.GetToken()
	if err != nil {
		return "3", err
	}
	fmt.Println("1--->", token)
	fmt.Println("2--->", dbUser.Token)
	tokenHash := ug.hmac.Hash(token)

	err = ug.db.Model(&User{}).Where("id = ?", dbUser.ID).Update("token", tokenHash).Error
	if err != nil {
		return "4", err
	}
	user.Token = token
	return token, nil
}
func (ug *userGorm) GetByToken(token string) (*User, error) {
	tokenHash := ug.hmac.Hash(token)
	var user User
	err := ug.db.Where("token = ?", tokenHash).First(&user).Error
	if err != nil {
		return nil, err
	}
	return &user, err
}

func (ug *userGorm) LogoutUser(user *User) error {
	return ug.db.Model(user).Where("id=?", user.ID).Update("token", "").Error

}

// package models

// import "github.com/jinzhu/gorm"

// type Image struct {
// 	gorm.Model
// 	Filename  string `gorm:"not null"`
// 	GalleryID uint   `gorm:"not null"`
// 	Status    bool
// }
// type ImageService interface {
// }
