package main

import (
	"gallery-api/config"
	"gallery-api/handlers"
	"gallery-api/middleWare"
	"gallery-api/models"
	"log"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

const hmacKey = "secret"

func main() {
	conf := config.Load()

	db, err := gorm.Open("mysql", conf.Connection)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	if conf.Mode == "dev" {
		db.LogMode(true)
	}

	if err := db.AutoMigrate(
		&models.Gallery{},
		&models.User{},
		&models.Image{},
	).Error; err != nil {
		log.Fatal(err)
	}

	gs := models.NewGalleryService(db)
	ims := models.NewImageService(db)
	us := models.NewUserService(db, conf.HMACKey)

	uh := handlers.NewUserHandler(us)
	gh := handlers.NewGalleryHandler(gs)
	imh := handlers.NewImageHandler(gs, ims)

	if conf.Mode != "dev" {
		gin.SetMode(gin.ReleaseMode)
	}

	r := gin.Default()
	config := cors.DefaultConfig()
	config.AllowAllOrigins = true
	config.AllowHeaders = []string{"Content-Type", "Authorization"}

	r.Static("/upload", "./upload")
	r.Use(cors.New(config))
	r.POST("/login", uh.Login)
	r.POST("/signup", uh.SignupUser)
	r.GET("/galleries", gh.ListPublish)

	auth := r.Group("/auth")
	auth.Use(middleWare.RequireUser(us))
	{
		auth.POST("/logout", uh.Logout)
		auth.POST("/galleries", gh.CreateGallery)
		auth.GET("/galleries", gh.ListGallery)
		auth.POST("/galleries/:id/images", imh.CreateImage)
		auth.GET("/galleries/:id/images", imh.ListGalleryImages)
		auth.DELETE("/images/:id", imh.DeleteImage)
		auth.DELETE("/galleries/:id", gh.DeleteGallery)
		auth.PATCH("/galleries/:id/name", gh.UpdateGallery)
		auth.PATCH("/galleries/:id/public", gh.UpdatePublishing)
		auth.GET("/galleries/:id", gh.GetOne)

	}
	r.Run()
}
