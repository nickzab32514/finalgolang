package handlers

import (
	"fmt"
	"gallery-api/context"
	"gallery-api/models"
	"strconv"

	"github.com/gin-gonic/gin"
)

type GalleryHandler struct {
	gs models.GalleryService
	// db *gorm.DB
}
type ListGallery struct {
	ID        uint       `json:"id"`
	Name      string     `json:"name"`
	IsPublish bool       `json:"is_publish"`
	Images    []ImageRes `json:"images"`
}
type CreateGallReq struct {
	Name string `json:"name"`
}
type CreateGallRes struct {
	ListGallery
}
type UpdateStatusGet struct {
	IsPublish bool `json:"is_publish"`
}

func NewGalleryHandler(gs models.GalleryService) *GalleryHandler {
	return &GalleryHandler{gs}
}
func (gh *GalleryHandler) CreateGallery(c *gin.Context) {
	user := context.User(c)
	if user == nil {
		c.Status(401)
		return
	}
	var dataGallery CreateGallReq //ใช้แค่ตรงนี้
	if err := c.BindJSON(&dataGallery); err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	fmt.Println("data", dataGallery)
	var gallery models.Gallery
	gallery.Name = dataGallery.Name
	gallery.UserID = user.ID
	if err := gh.gs.Create(&gallery); err != nil {
		fmt.Println("error=>", err)
		c.JSON(500, gin.H{
			"message": err,
		})
		return
	}
	res := new(CreateGallRes)
	res.ID = gallery.ID
	res.Name = gallery.Name
	res.IsPublish = gallery.IsPublish
	c.JSON(201, gin.H{
		"message": "succesful to create",
	})
}

func (gh *GalleryHandler) ListGallery(c *gin.Context) {
	user := context.User(c)
	if user == nil {
		c.Status(401)
		return
	}
	listGallery, err := gh.gs.ListGalleryUser(user.ID)
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}

	gallerys := []ListGallery{}
	for _, cg := range listGallery {
		gallerys = append(gallerys, ListGallery{
			ID:        cg.ID,
			Name:      cg.Name,
			IsPublish: cg.IsPublish,
		})
	}
	c.JSON(200, gallerys)
}

func (gh *GalleryHandler) ListPublish(c *gin.Context) {
	data, err := gh.gs.ListAllPublish()
	if err != nil {
		c.JSON(500, gin.H{
			"message": "id can not be empty",
		})
		return
	}
	galleries := []ListGallery{}
	for _, d := range data {
		images := []ImageRes{}
		for _, img := range d.Images {
			images = append(images, ImageRes{
				ID:        img.ID,
				GalleryID: img.GalleryID,
				Filename:  img.FilePath(),
			})
		}
		galleries = append(galleries, ListGallery{
			ID:        d.ID,
			Name:      d.Name,
			IsPublish: d.IsPublish,
			Images:    images,
		})
	}
	c.JSON(200, galleries)
}

// DeleteGallery -
func (gh *GalleryHandler) DeleteGallery(c *gin.Context) {
	fmt.Println("id======>")

	id, err := strconv.Atoi(c.Param("id"))
	//แปลงstring -->int
	fmt.Println(id)
	// var galleryies Gallery
	if err != nil {
		c.JSON(400, gin.H{
			"message": "id can not be empty",
		})
	}
	if err := gh.gs.Delete(uint(id)); err != nil {
		fmt.Println(err)

		c.JSON(500, gin.H{"message #": err.Error()})
		c.Status(204)
	}
}

func (gh *GalleryHandler) UpdateGallery(c *gin.Context) {

	id, err := strconv.Atoi(c.Param("id"))
	var gall models.Gallery
	if err := c.BindJSON(&gall); err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
	}
	// if err := c.BindJSON(gall); err != nil {
	// 	c.JSON(400, gin.H{
	// 		"message": err.Error(),
	// 	})
	// 	return
	// }

	getGall, err := gh.gs.GetByID(uint(id))

	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}

	getGall.Name = gall.Name
	fmt.Println(getGall)
	if err := gh.gs.UpdateGallery(getGall); err != nil {
		c.JSON(500, gin.H{
			"message": err,
		})
		return
	}

	c.Status(204)
}
func (gh *GalleryHandler) GetOne(c *gin.Context) {
	idStr := c.Param("id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err,
		})
		return
	}
	data, err := gh.gs.GetByID(uint(id))
	if err != nil {
		c.JSON(500, gin.H{
			"message": err,
		})
		return
	}
	c.JSON(200, ListGallery{
		ID:        data.ID,
		Name:      data.Name,
		IsPublish: data.IsPublish,
	})
}
func (gh *GalleryHandler) UpdatePublishing(c *gin.Context) {
	idStr := c.Param("id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		c.JSON(400, err)
		return
	}
	req := new(UpdateStatusGet)
	if err := c.BindJSON(req); err != nil {
		c.JSON(400, err)
		return
	}
	err = gh.gs.UpdateGalleryPublishing(uint(id), req.IsPublish)
	if err != nil {
		c.JSON(500, err)
		return
	}
	c.Status(204)
}
