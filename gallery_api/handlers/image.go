package handlers

import (
	"fmt"
	"gallery-api/models"
	"net/http"
	"path/filepath"
	"strconv"

	"github.com/gin-gonic/gin"
)

type ImageRes struct {
	ID        uint   `json:"id"`
	GalleryID uint   `json:"gallery_id"`
	Filename  string `json:"filename"`
}

type CreateImageRes struct {
	ImageRes
}

type ImageHandler struct {
	gs  models.GalleryService
	ims models.ImageService
}

func NewImageHandler(gs models.GalleryService, ims models.ImageService) *ImageHandler {
	return &ImageHandler{gs, ims}
}

func (imh *ImageHandler) CreateImage(c *gin.Context) {
	gallIDStr := c.Param("id")
	galleryID, err := strconv.Atoi(gallIDStr)
	if err != nil {
		c.JSON(400, gin.H{
			"message": "1",
		})
		return
	}

	gallery, err := imh.gs.GetByID(uint(galleryID))
	fmt.Println(gallIDStr)
	fmt.Println(gallery)

	if err != nil {
		c.JSON(400, gin.H{
			"message": "2",
		})
		return
	}

	form, err := c.MultipartForm()
	if err != nil {
		c.JSON(400, gin.H{
			"message": "3",
		})
		return
	}

	images, err := imh.ims.CreateImages(form.File["photos"], gallery.ID)
	if err != nil {
		c.JSON(500, gin.H{
			"message": "4",
		})
		return
	}

	res := []CreateImageRes{}
	for _, img := range images {
		r := CreateImageRes{}
		r.ID = img.ID
		r.GalleryID = gallery.ID
		r.Filename = filepath.Join(models.UploadPath, gallIDStr, img.Filename)
		res = append(res, r)
	}

	c.JSON(201, res)
}

func (imh *ImageHandler) DeleteImage(c *gin.Context) {
	imageIDStr := c.Param("id")
	id, err := strconv.Atoi(imageIDStr)
	if err != nil {
		c.JSON(400, gin.H{
			"message 6": err,
		})
		return
	}
	if err := imh.ims.DeleteImg(uint(id)); err != nil {
		c.JSON(500, gin.H{
			"message 7": err,
		})
		return
	}
	c.Status(200)
}

type ListGalleryImagesRes struct {
	ImageRes
}

func (imh *ImageHandler) ListGalleryImages(c *gin.Context) {
	gallIDStr := c.Param("id")
	id, err := strconv.Atoi(gallIDStr)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err,
		})
		return
	}

	gallery, err := imh.gs.GetByID(uint(id))
	if err != nil {
		c.JSON(400, gin.H{
			"message": err,
		})
		return
	}
	images, err := imh.ims.GetByGalleryID(gallery.ID)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err,
		})
		return
	}
	res := []ListGalleryImagesRes{}
	for _, img := range images {
		r := ListGalleryImagesRes{}
		r.ID = img.ID
		r.GalleryID = gallery.ID
		r.Filename = img.FilePath()
		res = append(res, r)
	}
	c.JSON(http.StatusOK, res)
}
