package main

import (
	"fmt"
	"log"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"golang.org/x/crypto/bcrypt"
)

type User struct {
	gorm.Model
	Email    string `gorm:"unique_index;not null"`
	Password string `gorm:"not null"`
	Token    string `gorm:"unique_index"`
}

func main() {
	db, err := gorm.Open("mysql", "root:password@tcp(127.0.0.1:3307)/galleryDB?charset=utf8&parseTime=true")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	db.LogMode(true)
	if err := db.AutoMigrate(
		&User{},
	).Error; err != nil {
		log.Fatal(err)
	}

	user := new(User)
	user.Email = "test@test"
	password := "1111"
	hash2, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.MinCost)
	if err != nil {
		fmt.Println("ERR 2", err)
	}
	user.Password = string(hash2)
	// db.Create(user)

	found := new(User)
	db.Where("email = ?", user.Email).First(found)
	fmt.Println(found)

	err = bcrypt.CompareHashAndPassword([]byte(found.Password), []byte(password))
	if err != nil {
		fmt.Println("ERR 1", err)
	}

	// hash := "$2a$12$oMEfX.KOxRQk/yf4DijhH.ziZ3eRJgsOXiDsIKXPOJ5HDSFYTsBXe"
	// password := "1111"
	// err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	// if err != nil {
	// 	fmt.Println("ERR 1", err)
	// }

	// hash2, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.MinCost)
	// if err != nil {
	// 	fmt.Println("ERR 2", err)
	// }

	// err = bcrypt.CompareHashAndPassword(hash2, []byte(password))
	// if err != nil {
	// 	fmt.Println("ERR 3", err)
	// }
}
