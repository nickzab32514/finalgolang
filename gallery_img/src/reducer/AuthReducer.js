const getToken = () => {
  return localStorage.getItem("token") || "";
};
export const intialLogin = {
  token: getToken(),
};
export const AuthReducer = (state = intialLogin, action) => {
  switch (action.type) {
    case "SET_LOGIN":
      return { ...state, token: action.token };
    default:
      return state;
  }
};
