import React, { useState, useEffect } from "react";
import "./App.css";
import { BrowserRouter, Route } from "react-router-dom";
import Login from "./page/Login";
import Signup from "./page/Signup";
import Images from "./page/Image";
import Home from "./page/Home";
import Gallery from "./page/Gallery";

function App() {
  return (
    <div>
      <BrowserRouter>
        <Route path="/" component={Home} exact={true} />
        <Route path="/galleries" component={Gallery} exact={true} />
        <Route path="/galleries/image" component={Images} exact={true} />
        <Route path="/Login" component={Login} exact={true} />

        <Route path="/signup" component={Signup} exact={true} />
      </BrowserRouter>
    </div>
  );
}

export default App;
