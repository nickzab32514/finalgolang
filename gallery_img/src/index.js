// import React, { Component } from "react";
// import ReactDOM from "react-dom";
// import "./index.css";
// import "antd/dist/antd.css";
// import "bootstrap/dist/css/bootstrap.css";
// import * as serviceWorker from "./serviceWorker";
// // import { BrowserRouter, Route } from "react-router-dom";
// // import Gallery from "./page/Gallery";
// // import Login from "./page/Login";
// // import Signup from "./page/Signup";
// // import Images from "./page/Image";
// import { Provider } from "react-redux";
// import { rootReducer } from "./reducer";

// import App from "./App.js";
// const store = createStore(rootReducer);

// const MainRouting = (
//   <BrowserRouter>
//     <Route path="/" component={App} exact={true} />
//     <Route path="/galleries" component={Gallery} />
//     <Route path="/image" component={Images} />
//     <Route path="/login" component={Login} />
//     <Route path="/signup" component={Signup} />
//   </BrowserRouter>
// );

// ReactDOM.render(MainRouting, document.getElementById("root"));
// // If you want your app to work offline and load faster, you can change
// // unregister() to register() below. Note this comes with some pitfalls.
// // Learn more about service workers: https://bit.ly/CRA-PWA
// serviceWorker.unregister();
import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App.js";
import * as serviceWorker from "./serviceWorker";
import { createStore } from "redux";
import { Provider } from "react-redux";
import { rootReducer } from "./reducer/rootReducer";
import "antd/dist/antd.css"; // or 'antd/dist/antd.less'
import "bootstrap/dist/css/bootstrap.css";
import { AuthReducer } from "./reducer/AuthReducer";

const appStore = createStore(AuthReducer);

ReactDOM.render(
  <Provider store={appStore}>
    <App />
  </Provider>,
  document.getElementById("root")
);

serviceWorker.unregister();
