import axios from "axios";
export const host = "http://localhost:8080";

export function createGallery(token, name) {
  const url = `${host}/auth/galleries`;
  return axios.post(url, name, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
}
export function signup(email, password) {
  return axios.post(`${host}/signup`, email, password);
}
export function listGalery() {
  return axios.get(`${host}/auth/galleries`);
}
export function deleteGallery(id, token) {
  return axios.delete(`${host}/auth/galleries/${id}`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
}
export function login({ email, password }) {
  const url = `${host}/login`;
  return axios.post(url, { email, password });
}
export function listPublishGallery(token) {
  const url = `${host}/galleries`;
  return axios.get(url, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
}

export function updateGalleryName({ id, name }) {
  const url = `${host}/galleries/${id}/names`;
  return axios.patch(url, { name });
}

export function updateGalleryStatus(token, { id, is_publish }) {
  return axios.patch(`${host}/galleries/${id}/public`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
}

export function getGallery(id) {
  return axios.get(`${host}/galleries/${id}`);
}

export function listGalleryImage(id) {
  return axios.get(`${host}/galleries/${id}/images`);
}

export function logout() {
  return axios.post(`${host}/logout`);
}

export function upload(id, formData, token) {
  return axios.post(`${host}/auth/galleries/${id}/images`, formData, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
}

export function deleteImage(id, formData, token) {
  return axios.delete(`/images/${id}`, formData, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
}
