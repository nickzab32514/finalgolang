import React from "react";
import "./App.css";
import { BrowserRouter, Route, Redirect } from "react-router-dom";
import Login from "./page/Login";
import Gallery from "./page/Gallery";
// import "antd/dist/antd.css"; // or 'antd/dist/antd.less'

const Routeing = () => {
  return (
    <BrowserRouter>
      <Route path="/login" component={Login} exact={true} />
      <Route path="/gallaries" component={Gallery} exact={true} />
    </BrowserRouter>
  );
};
export default Routeing;
