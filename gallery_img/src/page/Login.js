import React, { useState } from "react";
// import logo from "./logo.svg";
import "./Login.css";
import { Card, Row, Col } from "antd";
import { Form, Input, Button, Checkbox } from "antd";
import { MailOutlined, LockOutlined } from "@ant-design/icons";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { setToken } from "../action/AuthAction";
import { login } from "../api";
import canva from "../component/canva.jpg";
import axios from "axios";

function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const token = useSelector((state) => state.token);

  const dispatch = useDispatch();
  const history = useHistory();

  const handlelogin = async () => {
    console.log("email123", { email, password });
    console.log("password", password);

    await axios
      .post(`http://localhost:8080/login`, { email: email, password: password })
      .then((response) => {
        dispatch(setToken(response.data.token));
        localStorage.setItem("token", response.data.token);
        if (token != "") {
          console.log("sadasdasd: ", response);
          history.push("/galleries");
        }
      })
      .catch((err) => {
        console.log("Error", err);
      });

    // try {
    //   const response = await axios.post(`http://localhost:8080/login`, {
    //     email: email,
    //     password: password,
    //   });
    // } catch (err) {}

    // console.log("repo:: ", response);
  };
  // const onFinish = (values) => {
  //   console.log("Received values of form: ", values);
  // };
  return (
    <div id="form-wrapper" style={{ maxWidth: "500px", margin: "auto" }}>
      <Row style={{ marginTop: 182, marginLeft: 64, justifyContent: "center" }}>
        <Card id="cardLogin">
          <Row
            style={{
              textAlign: "center",
              justifyContent: "center",
              marginBottom: "32px",
              borderBottom: " 4px dotted",
              borderBottomColor: "#e1e1e1",
              paddingBottom: "16px",
            }}
          >
            <img
              src={canva}
              style={{ width: "30%", height: "30%" }}
              alt="Logo"
            />
            <h1 style={{ color: "#ff618dc7", marginTop: "30px" }}>Login</h1>
          </Row>
          <Form
            name="normal_login"
            className="login-form"
            initialValues={{ remember: true }}
            // onFinish={onFinish}
            onFinish={handlelogin}
          >
            <Form.Item
              name="Email"
              rules={[
                { required: true, message: "Please input your Username!" },
              ]}
            >
              <Input
                prefix={<MailOutlined className="site-form-item-icon" />}
                placeholder="Email"
                onChange={(e) => setEmail(e.target.value)}
              />
            </Form.Item>
            <Form.Item
              name="password"
              rules={[
                { required: true, message: "Please input your Password!" },
              ]}
            >
              <Input
                prefix={<LockOutlined className="site-form-item-icon" />}
                type="password"
                placeholder="Password"
                onChange={(e) => setPassword(e.target.value)}
              />
            </Form.Item>
            <Form.Item>
              <Button
                type="primary"
                htmlType="submit"
                className="login-form-button"
                style={{ marginTop: "32px" }}
              >
                Log in
              </Button>
            </Form.Item>
            <Form.Item>
              Need an account ? <a href="/signup">register now!</a>
            </Form.Item>
          </Form>
        </Card>
      </Row>
      {/* <input
        value={email}
        type="email"
        onChange={(e) => setEmail(e.target.value)}
      />
      <input
        value={password}
        type="password"
        onChange={(e) => setPassword(e.target.value)}
      />
      <button style={{ marginTop: "32px" }} onClick={handlelogin}>
        Log in
      </button> */}
    </div>
    // </div>
  );
}

export default Login;
