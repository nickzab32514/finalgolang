import React from "react";
import { Switch, Route, useRouteMatch } from "react-router-dom";

import Image from "../page/Image";
import Gallery from "../page/Gallery";

export default function AdminPage() {
  const { pathStatic } = useRouteMatch();

  return (
    <div>
      <Switch>
        <Route exact path={pathStatic}>
          <Image />
        </Route>
        <Route path={`${pathStatic}/galleries/:id`}>
          <Gallery />
        </Route>
      </Switch>
    </div>
  );
}
