// import React, { useEffect, useState } from "react";
// import PropTypes from "prop-types";
// import { makeStyles } from "@material-ui/core/styles";
// import GridList from "@material-ui/core/GridList";
// import GridListTile from "@material-ui/core/GridListTile";
// import Card from "@material-ui/core/Card";
// import CardActions from "@material-ui/core/CardActions";
// import CardContent from "@material-ui/core/CardContent";
// import Button from "@material-ui/core/Button";
// import Modal from "@material-ui/core/Modal";
// import DeleteIcon from "@material-ui/icons/Delete";
// import "./gallery.css";
// import Backdrop from "@material-ui/core/Backdrop";
// import Fade from "@material-ui/core/Fade";
// import dark from "../component/dark.jpg";
// import HeaderImage from "../component/HeaderImage";
// import AddIcon from "@material-ui/icons/Add";
// import { GridListTileBar, IconButton } from "@material-ui/core";
// import Addphoto from "../component/Addphoto";
// const useStyles = makeStyles((theme) => ({
//   gridList: {
//     width: 400,
//     height: 200,
//   },
//   title: {
//     color: theme.palette.primary.light,
//   },
//   icons: {
//     color: "white",
//   },
//   icon: {
//     color: "#ab38f3",
//   },
//   titleBar: {
//     background:
//       "linear-gradient(180deg, rgba(14,0,255,0.03154765324098385) 0%, rgba(84,84,99,0.6533963927367823) 100%)",
//   },
//   button: {
//     marginLeft: 16,
//     border: "1px solid",
//     color: "#f50057",
//     borderColor: "#f50057",
//   },
//   buttonss: {
//     marginLeft: 16,
//     border: "2px solid",
//     color: " #fff",
//     borderColor: "#fff",
//     borderRadius: 80,
//   },
//   root: {
//     border: " solid 2px #ab38f3",
//     textAlign: "center",
//   },
//   paper: {
//     position: "absolute",
//     width: 500,
//     backgroundColor: theme.palette.background.paper,
//     border: "2px solid #f50057",
//     boxShadow: theme.shadows[5],
//     padding: theme.spacing(2, 4, 3),
//   },
// }));
// function rand() {
//   return Math.round(Math.random() * 20) - 10;
// }
// function getModalStyle() {
//   const top = 50 + rand();
//   const left = 50 + rand();

//   return {
//     top: `${top}%`,
//     left: `${left}%`,
//     transform: `translate(-${top}%, -${left}%)`,
//   };
// }
// export default function Image() {
//   const classes = useStyles();
//   const [modalStyle] = React.useState(getModalStyle);
//   const [modalIsOpen, setModalIsOpen] = useState(false);

//   // const [loading, setLoading] = useState(false);
//   const [open, setOpen] = React.useState(false);
//   const toggleModal = () => {
//     setModalIsOpen(true);
//   };
//   const showModal = () => {
//     setOpen(true);
//   };
// const handleOk = () => {
//   // setLoading(true);
//   setTimeout(() => {
//     // setLoading(false);
//     setVisible1(false);
//   }, 3000);
// };
// const handleCancel = () => {
//   setOpen(false);
// };
// const [arrGallery, setArrGallery] = useState([]);
// const [gallery, setGallery] = useState("");

// const handleSubmit = (e) => {
//   e.preventDefault();
//   if (gallery.trim() === "") {
//     return alert("Not space bar");
//   }
//   createGallery({ name: gallery })
//     .then((resp) => {
//       // setArrGallery([...arrGallery, resp.data]);
//       console.log("arr=>", arrGallery);
//       fetchData();
//       setGallery("");
//     })
//     .catch((err) => console.error(err));
// };
// const handleDelete = async (gallery, event) => {
//   event.stopPropagation();
//   await deleteGallery(gallery.id);
//   fetchData();
// };
// useEffect(() => {
//   fetchData();
// }, []);
// const fetchData = () => {
//   listGalery()
//     .then((data) => {
//       setArrGallery(data.data);
//       console.log("data=>", data.data);
//     })
//     .catch((err) => console.log(err));
// };

//   return (
//     <div>
//       <HeaderImage />
//       <div
//         style={{
//           marginLeft: "6%",
//           marginRight: "6%",
//         }}
//       >
//         <div className="row" id="rr">
//           <button
//             type="button"
//             style={{ background: "#ab38f300", width: 400, height: 60 }}
//             className={classes.buttonss}
//             onClick={showModal}
//           >
//             Add Image
//           </button>
//           <Modal
//             aria-labelledby="transition-modal-title"
//             aria-describedby="transition-modal-description"
//             className={classes.modal}
//             open={open}
//             onClose={handleCancel}
//             closeAfterTransition
//             BackdropComponent={Backdrop}
//             BackdropProps={{
//               timeout: 500,
//             }}
//           >
//             <Fade in={open}>{body}</Fade>
//           </Modal>
//         </div>
//         <GridList
//           cellHeight={350}
//           // spacing={5}
//           cols={5}
//           style={{
//             margin: "128px !important",
//             padding: "4px",
//           }}
//         >
//           {arrGallery.map((t, index) => {
//             console.log("ssssss");
//             return (
//               <GridListTile
//                 key={t.img}
//                 cols={1}
//                 id="img"
//                 style={{
//                   marginTop: 16,
//                   marginBottom: 8,
//                   padding: 16,
//                 }}
//               >
//                 <img src={dark} class="image" />

//                 <GridListTileBar
//                   style={{ textAlign: "right" }}
//                   title={t.name}
//                   classes={{
//                     root: classes.titleBar,
//                   }}
//                 />
//               </GridListTile>
//             );
//           })}
//         </GridList>
//       </div>
//     </div>
//   );
// }
import React, { useEffect, useState } from "react";
import HeaderImg from "../component/HeaderImage";
import axios from "axios";

import { Upload, Button, message, Space } from "antd";
import { UploadOutlined } from "@ant-design/icons";
import { useSelector } from "react-redux";
import { upload } from "../api";

function Image(props) {
  const [message, setMessage] = useState("");
  const [images, setImages] = useState([]);
  const token = useSelector((state) => state.token);

  const [antImages, setAntImages] = useState([]);

  const success = () => {
    message.success("This is a success message");
  };

  const error = () => {
    message.error("This is an error message");
  };
  useEffect(() => {
    fetchImage();
  }, []);
  const fetchImage = () => {
    axios
      .get(
        `http://localhost:8080/auth/galleries/${props.location.state.gallID}/images`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      )
      .then((res) => {
        setImages(res.data);
      })
      .catch((err) => console.error(err));
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const files = e.target.elements["photos"].files;
    const form = new FormData();
    for (let i = 0; i < files.length; i++) {
      form.append("photos", files[i]);
    }
    console.log(form);
    let id = props.location.state.gallID;
    upload(id, form, token)
      .then((res) => {
        console.log(res);
        fetchImage();
      })
      .catch((err) => {
        error();
        console.error(err);
      });
  };

  const handleUpload = () => {
    console.log(antImages);
    const form = new FormData();
    for (let i = 0; i < antImages.length; i++) {
      form.append("photos", antImages[i]);
    }
    axios
      .post(
        `http://localhost:8080/auth/galleries/${props.location.state.gallID}/images`,
        form,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      )
      .then((res) => {
        setImages(res.data);
        fetchImage();
      })
      .catch((err) => {
        console.error(err);
      });
  };

  console.log(images);
  return (
    <div className="App">
      <HeaderImg />
      {message}
      <div style={{ marginLeft: "6%", marginRight: "6%" }}>
        <Space>
          <Upload
            multiple
            onChange={({ file, fileList, event }) => {
              console.log("file", file);
              console.log("fileList", fileList);
              console.log("event", event);
              setAntImages(fileList.map((f) => f.originFileObj));
            }}
          >
            <Button>
              <UploadOutlined /> Click to Upload
            </Button>
          </Upload>
        </Space>
        <Button onClick={handleUpload}>Upload Ant</Button>
      </div>
      <div
        style={{ display: "flex", flexFlow: "column wrap", height: "500px" }}
      >
        {images.map((img, index) => (
          <div style={{ width: "20%" }} key={index}>
            <img
              src={`http://localhost:8080/${img.filename}`}
              style={{ width: "100%" }}
            />
            <button
              onClick={() => {
                axios
                  .delete(`http://localhost:8080/auth/images/${img.id}`, {
                    headers: {
                      Authorization: `Bearer ${token}`,
                    },
                  })
                  .then(() => {
                    fetchImage();
                  })
                  .catch((err) => {
                    console.error(err);
                  });
              }}
            >
              delete
            </button>
          </div>
        ))}
      </div>
    </div>
  );
}

export default Image;
