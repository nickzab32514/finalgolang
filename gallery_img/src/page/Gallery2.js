import React, { useEffect, useState } from "react";
// import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import GridList from "@material-ui/core/GridList";
import GridListTile from "@material-ui/core/GridListTile";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import Modal from "@material-ui/core/Modal";
import DeleteIcon from "@material-ui/icons/Delete";
import { createGallery, listGalery, deleteGallery } from "../api";
import "./gallery.css";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";

import Header from "../component/Headers";
import AddIcon from "@material-ui/icons/Add";
import { GridListTileBar, IconButton } from "@material-ui/core";
import { useSelector } from "react-redux";
// import Addphoto from "../component/Addphoto";

const useStyles = makeStyles((theme) => ({
  gridList: {
    width: 400,
    height: 200,
  },
  title: {
    color: theme.palette.primary.light,
  },
  icons: {
    color: "white",
  },
  icon: {
    color: "#ab38f3",
  },
  titleBar: {
    background:
      "linear-gradient(180deg, rgba(14,0,255,0.03154765324098385) 0%, rgba(84,84,99,0.6533963927367823) 100%)",
  },
  button: {
    marginLeft: 16,
    border: "1px solid",
    color: "#f50057",
    borderColor: "#f50057",
  },
  buttonss: {
    marginLeft: 16,
    border: "1px solid",
    color: " #ab38f3",
    borderColor: "#ab38f3",
  },
  root: {
    border: " solid 2px #ab38f3",
    textAlign: "center",
  },
  paper: {
    position: "absolute",
    width: 500,
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #f50057",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));
function rand() {
  return Math.round(Math.random() * 20) - 10;
}
function getModalStyle() {
  const top = 50 + rand();
  const left = 50 + rand();

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}
export default function Gallery2() {
  const classes = useStyles();
  const [modalStyle] = React.useState(getModalStyle);
  const token = useSelector((state) => state.token);
  // const [loading, setLoading] = useState(false);
  const [open, setOpen] = React.useState(false);

  const showModal = () => {
    setOpen(true);
  };
  const handleCancel = () => {
    setOpen(false);
  };
  const [arrGallery, setArrGallery] = useState([]);
  const [gallery, setGallery] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    if (gallery.trim() === "") {
      return alert("Not space bar");
    }
    createGallery({ name: gallery }, token)
      .then((resp) => {
        // setArrGallery([...arrGallery, resp.data]);
        console.log("arr=>", arrGallery);
        setArrGallery([...gallery, resp.data]);
      })
      .catch((err) => console.error(err));
  };
  const handleDelete = async (gallery, event) => {
    event.stopPropagation();
    await deleteGallery(gallery.id);
    fetchData();
  };
  // useEffect(() => {
  //   fetchData();
  // }, []);
  const fetchData = () => {
    // listGalery()
    //   .then((data) => {
    //     setArrGallery(data.data);
    //     console.log("data=>", data.data);
    //   })
    //   .catch((err) => console.log(err));
  };
  const body = (
    <Fade in={open}>
      <div style={modalStyle} className={classes.paper}>
        <h2 id="simple-modal-title" style={{ color: "#f50057" }}>
          Create Gallery
        </h2>
        <input
          className="btn"
          id="query"
          placeholder="input name"
          onChange={(e) => setGallery(e.target.value)}
          autoFocus
        />
        <Button
          variant="outlined"
          color="secondary"
          style={{ background: "#fff", marginTop: 0 }}
          className={classes.button}
          onClick={handleSubmit}
        >
          Submit
        </Button>
        <Button
          variant="outlined"
          color="secondary"
          style={{ background: "#fff", marginTop: 0 }}
          className={classes.button}
          onClick={handleCancel}
        >
          Cancel
        </Button>
      </div>
    </Fade>
  );
  return (
    <div>
      <Header />
      <h1>{token}</h1>
      <div
        style={{
          marginLeft: "6%",
          marginRight: "6%",
        }}
      >
        <GridList
          cellHeight={180}
          spacing={4}
          cols={3}
          style={{
            background: "rgba(255,255,255,0.5301470930168943)",
            borderRadius: 10,
            margin: "128px !important",
            padding: "24px",
          }}
        >
          <Card className={classes.root} variant="outlined">
            <AddIcon style={{ fontSize: 120 }} className={classes.icon} />
            <CardActions>
              <button
                type="button"
                style={{ background: "#fff", marginLeft: 150 }}
                className={classes.buttonss}
                onClick={showModal}
              >
                Add Gallery
              </button>
              <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                className={classes.modal}
                open={open}
                onClose={handleCancel}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                  timeout: 500,
                }}
              >
                <Fade in={open}>{body}</Fade>
              </Modal>
            </CardActions>
          </Card>
          {arrGallery.map((t, index) => {
            console.log("ssssss");
            return (
              <GridListTile
                key={t.img}
                cols={1}
                style={{
                  marginTop: 16,
                  marginBottom: 8,
                  padding: 16,
                }}
              >
                <GridListTileBar
                  style={{ textAlign: "right" }}
                  title={t.name}
                  classes={{
                    root: classes.titleBar,
                  }}
                  actionIcon={
                    <IconButton
                      aria-label={`star ${t.title}`}
                      onClick={(event) => {
                        handleDelete(t, event);
                      }}
                    >
                      <DeleteIcon className={classes.icons} />
                    </IconButton>
                  }
                />
                />
              </GridListTile>
            );
          })}
        </GridList>
      </div>
    </div>
  );
}
