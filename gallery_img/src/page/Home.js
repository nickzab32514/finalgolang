import React, { useState, useEffect } from "react";
import { listPublishGallery } from "../api";
import { Layout, Menu, Card, Tag } from "antd";
import "../page/home.css";
import Nav from "./Nav";
const { Header, Content } = Layout;

export default function Home() {
  const [galleries, setGalleries] = useState([]);
  useEffect(() => {
    listPublishGallery()
      .then((res) => setGalleries(res.data))
      // console.log(galleries)
      .catch((err) => console.error(err));
  }, []);

  return (
    <div className="gallery-home">
      <Nav />

      <Layout>
        <Content
          className="site-layout"
          style={{ padding: "0 50px", marginTop: 76, marginBottom: 76 }}
        >
          <div
            className="site-layout-background"
            style={{ padding: 24, minHeight: 380 }}
          >
            {galleries.map((gal) => (
              <div key={gal.id}>
                <Tag color="orange">
                  <h1>{gal.name}</h1>
                </Tag>
                <div>
                  {gal.images.map((img) => (
                    <div key={img.id}>
                      <img src={img.filename} alt={img.filename} />
                    </div>
                  ))}
                </div>
              </div>
            ))}
          </div>
        </Content>
      </Layout>
    </div>
  );
}
