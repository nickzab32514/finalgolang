import React, { useEffect, useState } from "react";
// import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import GridList from "@material-ui/core/GridList";
import GridListTile from "@material-ui/core/GridListTile";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import Modal from "@material-ui/core/Modal";
import DeleteIcon from "@material-ui/icons/Delete";
import {
  createGallery,
  deleteGallery,
  listGalery,
  updateGalleryStatus,
} from "../api";
// import "./gallery.css";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";
import PublicIcon from "@material-ui/icons/Public";
import AddIcon from "@material-ui/icons/Add";
import { GridListTileBar, IconButton } from "@material-ui/core";
import { useSelector } from "react-redux";
import axios from "axios";
import Header from "../component/Headers";
import { Link } from "react-router-dom";
import photo from "../component/photo2.png";
import { useHistory } from "react-router-dom";
import { Input } from "antd";
const { Search } = Input;

const useStyles = makeStyles((theme) => ({
  gridList: {
    width: 400,
    height: 200,
  },
  title: {
    color: theme.palette.primary.light,
  },
  icons: {
    color: "white",
  },
  icon: {
    color: "#ab38f3",
  },
  titleBar: {
    background:
      "linear-gradient(0deg, rgba(0,0,0,1) 0%, rgba(255,255,255,0.8858893899356618) 100%)",
  },
  button: {
    marginLeft: 16,
    border: "1px solid",
    color: "#f50057",
    borderColor: "#f50057",
  },
  buttonss: {
    marginLeft: 16,
    border: "0px solid",
    color: " #ab38f3",
    borderColor: "#ab38f3",
  },
  root: {
    border: " solid 2px #ab38f3",
    textAlign: "center",
    boxShadow: "0 4px 4px 0 rgba(0, 0, 0, 0.16), 0 0 0 1px rgba(0, 0, 0, 0.08)",
  },
  paper: {
    position: "absolute",
    width: 500,
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #f50057",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

function Gallery() {
  const [nameGallery, setNameGallery] = useState("");
  const token = useSelector((state) => state.token);
  const [arrGallery, setArrGallery] = useState([]);
  const [is_publish, setIspublish] = useState(false);
  let history = useHistory();

  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const [modalStyle] = React.useState(getModalStyle);

  function rand() {
    return Math.round(Math.random() * 20) - 10;
  }
  function getModalStyle() {
    const top = 50 + rand();
    const left = 50 + rand();

    return {
      top: `${top}%`,
      left: `${left}%`,
      transform: `translate(-${top}%, -${left}%)`,
    };
  }

  const showModal = () => {
    setOpen(true);
  };
  const handleCancel = () => {
    setOpen(false);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (nameGallery.trim() === "") {
      return alert("Not space bar");
    }
    createGallery(token, { name: nameGallery })
      .then((resp) => {
        console.log("arr=>", arrGallery);
        setArrGallery([...arrGallery, resp.data]);
        console.log("arr=>", arrGallery);
      })
      .catch((err) => console.error(err));
    setNameGallery("");
  };
  const ButtonSearchName = (event) => {
    let Keyword = event.target.value;
    setNameGallery(Keyword);
  };

  const handleDelete = async (id, event) => {
    event.stopPropagation();

    deleteGallery(id, token)
      .then((resp) => {
        console.log("comlete success");
        listOneUserGallery();
      })
      .catch((err) => console.log("error", err));
  };

  useEffect(() => {
    listOneUserGallery();
  }, []);

  const listOneUserGallery = () => {
    axios
      .get(`http://localhost:8080/auth/galleries`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((response) => {
        console.log("responseeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee", response);
        setArrGallery(response.data);
      })
      .catch((err) => {
        console.log("errrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr", err);
      });
  };

  const updateStatus = (galleryID, is_public) => {
    const id = galleryID;

    updateGalleryStatus(id, { is_publish: !arrGallery.is_publish }, token)
      .then(() => {
        const update = arrGallery.map((gal) => {
          if (gal.id === id) {
            return { ...gal, is_publish: !is_publish };
          }
          return gal;
        });
        setArrGallery(update);
        listOneUserGallery();
      })
      .catch((err) => {
        console.log("errrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr", err);
      });
  };

  const body = (
    <Fade in={open}>
      <div style={modalStyle} className={classes.paper}>
        <h2 id="simple-modal-title" style={{ color: "#f50057" }}>
          Create Gallery
        </h2>
        <input
          className="btn"
          id="query"
          placeholder="input name"
          onChange={(e) => setNameGallery(e.target.value)}
          value={nameGallery}
          autoFocus
        />
        <Button
          variant="outlined"
          color="secondary"
          style={{ background: "#fff", marginTop: 0 }}
          className={classes.button}
          onClick={handleSubmit}
        >
          Submit
        </Button>
        <Button
          variant="outlined"
          color="secondary"
          style={{ background: "#fff", marginTop: 0 }}
          className={classes.button}
          onClick={handleCancel}
        >
          Cancel
        </Button>
      </div>
    </Fade>
  );

  return (
    <div>
      <Header />
      {/* <h1>{token}</h1> */}
      <div
        style={{
          marginLeft: "6%",
          marginRight: "6%",
        }}
      >
        <GridList
          cellHeight={250}
          spacing={4}
          cols={3}
          style={{
            background: "rgba(255,255,255,0.5301470930168943)",
            borderRadius: 10,
            margin: "128px !important",
            padding: "24px",
            boxShadow:
              "0 8px 8px 0 rgba(0, 0, 0, 0.16), 0 0 0 2px rgba(0, 0, 0, 0.08)",
          }}
        >
          <div
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
          ></div>
          <div
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <Search
              placeholder=" search "
              onChange={(e) => ButtonSearchName(e)}
              style={{
                color: "white",
                width: 280,
                borderRadius: 90,
              }}
            />
          </div>

          <Card className={classes.root} variant="outlined" id="card">
            <AddIcon
              style={{ fontSize: 120, marginTop: 20 }}
              className={classes.icon}
            />
            <CardActions>
              <button
                type="button"
                style={{ background: "#fff", marginLeft: 150 }}
                className={classes.buttonss}
                onClick={showModal}
              >
                Add Gallery
              </button>
              <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                className={classes.modal}
                open={open}
                onClose={handleCancel}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                  timeout: 500,
                }}
              >
                <Fade in={open}>{body}</Fade>
              </Modal>
            </CardActions>
          </Card>
          {arrGallery.map((t, index) => {
            console.log("ssssss");
            return (
              <GridListTile
                cols={1}
                style={{
                  marginTop: 16,
                  marginBottom: 8,
                  padding: 16,
                }}
                onClick={() =>
                  history.push("/galleries/image", { gallID: t.id })
                }
              >
                <div
                  style={{
                    background: "#fff",
                    textAlign: "center",
                    padding: 16,
                  }}
                >
                  <img
                    src={photo}
                    style={{
                      backgroundColor: "#fff",
                      width: "50%",
                      height: "50%",
                    }}
                  />
                </div>
                <GridListTileBar
                  style={{ textAlign: "right" }}
                  title={t.name}
                  classes={{
                    root: classes.titleBar,
                  }}
                  actionIcon={
                    <div>
                      <IconButton
                        aria-label={`star ${t.title}`}
                        onClick={(event) => {
                          handleDelete(t.id, event);
                        }}
                      >
                        <DeleteIcon className={classes.icons} />
                      </IconButton>
                      {t.is_public ? (
                        <label>Public</label>
                      ) : (
                        <label>Pivate</label>
                      )}

                      <IconButton
                        aria-label={`star ${t.title}`}
                        onClick={() => {
                          updateStatus(t.id, t.is_public);
                        }}
                      >
                        <PublicIcon className={classes.icons} />
                      </IconButton>
                    </div>
                  }
                />
              </GridListTile>
            );
          })}
        </GridList>
      </div>
    </div>
  );
}
export default Gallery;
