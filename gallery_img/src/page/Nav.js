import React from "react";
// import Header from "../component/Header";
import { Link, useHistory } from "react-router-dom";

import { Layout, Menu, Card } from "antd";
import "../page/home.css";
import canva from "../component/canva.jpg";

const { Header } = Layout;

export default function Nav() {
  const history = useHistory();
  return (
    <div className="gallery-home">
      <div className="logo">
        <img src={canva} style={{ width: "30%", height: "30%" }} alt="Logo" />
      </div>
      <Header
        style={{
          zIndex: 1,
          width: "100%",
          borderBottom: "3px solid #fff",
          padding: "5",
        }}
      >
        <Menu
          style={{ textAlign: "center" }}
          theme="dark"
          mode="horizontal"
          defaultSelectedKeys={["2"]}
        >
          <Menu.Item key="1" href="/">
            Home
          </Menu.Item>
          <Menu.Item key="2" onClick={() => history.push("/login")}>
            Login
          </Menu.Item>
          <Menu.Item key="3" onClick={() => history.push("/signup")}>
            Signup
          </Menu.Item>
        </Menu>
      </Header>
    </div>
  );
}
