import React, { useState } from "react";
// import logo from "./logo.svg";
import "./Login.css";
import { Card, Row, Col } from "antd";
import { Form, Input, Button, Checkbox } from "antd";
import { UserOutlined, LockOutlined, MailOutlined } from "@ant-design/icons";
// import { login } from "../api";
import canva from "../component/canva.jpg";
import { signup } from "../api";
import { useHistory } from "react-router-dom";

function Signup() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [username, setUsername] = useState("");
  const history = useHistory();
  // const dispatch = useDispatch();
  // const history = useHistory();
  // const handlelogin = (event) => {
  //   event.preventDefault();
  //   var formData = new FormData();
  //   formData.append("email", email);
  //   formData.append("password", password);

  //   login(formData)
  //     .then((res) => {
  //       console.log("res", res);
  //       dispatch(setLogin(res.data.token));
  //       history.push("/gallaries");
  //     })
  //     .catch((err) => {
  //       console.log("err", err);
  //     });
  // };
  const onSignup = () => {
    signup({ email, password });
    setPassword("");
    setEmail("");
    console.log(email, password);
    history.push("/");
  };
  const onFinish = (values) => {
    console.log("Received values of form: ", values);
  };
  return (
    <div id="form-wrapper" style={{ maxWidth: "500px", margin: "auto" }}>
      <Row style={{ marginTop: 182, marginLeft: 64, justifyContent: "center" }}>
        <Card id="cardLogin">
          <Row
            style={{
              textAlign: "center",
              justifyContent: "center",
              marginBottom: "32px",
              borderBottom: " 4px dotted",
              borderBottomColor: "#e1e1e1",
              paddingBottom: "16px",
            }}
          >
            <img
              src={canva}
              style={{ width: "30%", height: "30%" }}
              alt="Logo"
            />
            <h1 style={{ color: "#ff618dc7", marginTop: "30px" }}>Signup</h1>
          </Row>
          <Form
            name="normal_login"
            className="login-form"
            initialValues={{ remember: true }}
            onFinish={onSignup}
            href="/login"
          >
            {/* <Form.Item
              name="username"
              rules={[
                { required: true, message: "Please input your Username!" },
              ]}
            >
              <Input
                prefix={<UserOutlined className="site-form-item-icon" />}
                placeholder="Username"
              />
            </Form.Item> */}
            <Form.Item
              name="Email"
              rules={[{ required: true, message: "Please input your Email!" }]}
            >
              <Input
                prefix={<MailOutlined className="site-form-item-icon" />}
                placeholder="Email"
                onChange={(e) => setEmail(e.target.value)}
                type="Email"
              />
            </Form.Item>
            <Form.Item
              name="password"
              rules={[
                { required: true, message: "Please input your Password!" },
              ]}
            >
              <Input
                prefix={<LockOutlined className="site-form-item-icon" />}
                type="password"
                placeholder="Password"
                onChange={(e) => setPassword(e.target.value)}
              />
            </Form.Item>
            <Form.Item>
              <Button
                type="primary"
                htmlType="submit"
                className="login-form-button"
                style={{ marginTop: "32px" }}
                onClick={() => history.useHistory("/login")}
              >
                Sign up
              </Button>
            </Form.Item>
            <Form.Item>
              Aready have an account? <a href="/signup">Login now!</a>
            </Form.Item>
          </Form>
        </Card>
      </Row>
    </div>
    // </div>
  );
}

export default Signup;
