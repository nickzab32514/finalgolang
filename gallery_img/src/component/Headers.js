import React, { useState, useEffect } from "react";
import Button from "@material-ui/core/Button";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import dark from "./dark.jpg";
import { makeStyles } from "@material-ui/core/styles";
import AccountCircle from "@material-ui/icons/AccountCircle";
import "./head.css";
const useStyles = makeStyles((theme) => ({
  icon: {
    color: "#fff",
  },
}));

export default function Header() {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const handleClose = () => {
    setAnchorEl(null);
  };
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  return (
    <div>
      <div
        className="jumbotron jumbotron-fluid"
        style={{
          backgroundImage: `url(${dark})`,
          width: "100%",
        }}
      >
        <nav className="navbar sticky-top" id="nav">
          <AccountCircle
            style={{ fontSize: 48 }}
            className={classes.icon}
            onClick={handleClick}
          />
          <Menu
            id="simple-menu"
            anchorEl={anchorEl}
            keepMounted
            open={Boolean(anchorEl)}
            onClose={handleClose}
          >
            <MenuItem onClick={handleClose}>Gallery</MenuItem>
            <MenuItem onClick={handleClose}>Logout</MenuItem>
          </Menu>
          <form className="form-inline">
            <input
              className="form-control mr-sm-2"
              type="search"
              placeholder="Search"
              aria-label="Search"
            />
            <button
              className="btn btn-outline-danger my-2 my-sm-0"
              type="submit"
            >
              Search
            </button>
          </form>
        </nav>
        <div className="d-flex justify-content-center">
          <div
            className="row"
            style={{
              background: "rgba(255,255,255,0.4921638997395833)",
              padding: "12px 200px",
              boxShadow: "0 2px 2px 0 #d8d1d1, 0 0 0 1px #d8d1d1",
            }}
          >
            <h1 style={{ fontSize: 64 }}>Gallery</h1>
          </div>
        </div>
      </div>
    </div>
  );
}
