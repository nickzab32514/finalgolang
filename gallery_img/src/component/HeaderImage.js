import React, { useState, useEffect } from "react";
import Button from "@material-ui/core/Button";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import forest from "./tran.jpg";
import { makeStyles } from "@material-ui/core/styles";
import AccountCircle from "@material-ui/icons/AccountCircle";
import "./head.css";
import { useHistory } from "react-router-dom";
const useStyles = makeStyles((theme) => ({
  icon: {
    color: "#fff",
  },
}));

export default function Header() {
  const classes = useStyles();
  const history = useHistory();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const handleClose = () => {
    setAnchorEl(null);
  };
  const handleCloseGall = () => {
    setAnchorEl(null);
    history.push("/galleries");
  };
  const handleCloseLogout = () => {
    setAnchorEl(null);
    history.push("/galleries");
  };
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  return (
    <div>
      <div
        className="jumbotron jumbotron-fluid"
        style={{
          backgroundImage: `url(${forest})`,
          width: "100%",
        }}
      >
        <nav class="navbar sticky-top" id="nav">
          <AccountCircle
            style={{ fontSize: 48 }}
            className={classes.icon}
            onClick={handleClick}
          />
          <Menu
            id="simple-menu"
            anchorEl={anchorEl}
            keepMounted
            open={Boolean(anchorEl)}
            onClose={handleClose}
          >
            <MenuItem onClick={handleCloseGall}>Gallery</MenuItem>
            <MenuItem onClick={handleCloseLogout}>Logout</MenuItem>
          </Menu>
        </nav>
        <div className="d-flex justify-content-center">
          <div
            className="row"
            style={{
              background: "rgba(255,255,255,0.4921638997395833)",
              padding: "12px 200px",
              boxShadow: "0 2px 2px 0 #d8d1d1, 0 0 0 1px #d8d1d1",
            }}
          >
            <h1 style={{ fontSize: 48 }}>Image</h1>
          </div>
        </div>
      </div>
    </div>
  );
}
