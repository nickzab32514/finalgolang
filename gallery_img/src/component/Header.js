import React, { useState, useEffect } from "react";
import { Button } from "antd";
import dark from "./dark.jpg";
import { makeStyles } from "@material-ui/core/styles";
import AccountCircle from "@material-ui/icons/AccountCircle";
import "./head.css";
const useStyles = makeStyles((theme) => ({
  icon: {
    color: "red",
  },
}));
export default function Header() {
  const classes = useStyles();
  return (
    <div>
      {/* <nav class="navbar sticky-top navbar-light bg-light">
        <AccountCircle style={{ fontSize: 64 }} className={classes.icon} />

        <a class="navbar-brand" href="#">
          Sticky top
        </a>
        <form class="form-inline">
          <input
            class="form-control mr-sm-2"
            type="search"
            placeholder="Search"
            aria-label="Search"
          />
          <button class="btn btn-outline-success my-2 my-sm-0" type="submit">
            Search
          </button>
        </form>
      </nav> */}
      <div
        className="jumbotron jumbotron-fluid"
        style={{
          backgroundImage: `url(${dark})`,
        }}
      >
        <nav class="navbar sticky-top navbar-light bg-light" id="nav">
          <AccountCircle style={{ fontSize: 48 }} className={classes.icon} />

          <a class="navbar-brand" href="#">
            Sticky top
          </a>
          <form class="form-inline">
            <input
              class="form-control mr-sm-2"
              type="search"
              placeholder="Search"
              aria-label="Search"
            />
            <button class="btn btn-outline-danger my-2 my-sm-0" type="submit">
              Search
            </button>
          </form>
        </nav>
        <div className="d-flex justify-content-center">
          <div
            className="row"
            style={{ background: "#fff", padding: "12px 200px" }}
          >
            <h1 style={{ fontSize: 64 }}>GALLERY</h1>
          </div>
        </div>
      </div>
    </div>
  );
}
