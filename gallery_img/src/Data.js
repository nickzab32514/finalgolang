const Data = [
  {
    img:
      "https://images.unsplash.com/photo-1494537176433-7a3c4ef2046f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1267&q=80",
    name: "Breakfast",
    author: "jill111",
    cols: 2,
    featured: true,
  },
  {
    img:
      "https://images.unsplash.com/photo-1498569026542-39c64353e401?ixlib=rb-1.2.1&auto=format&fit=crop&w=1267&q=80",
    name: "Tasty burger",
    author: "director90",
  },
];
export default Data;
