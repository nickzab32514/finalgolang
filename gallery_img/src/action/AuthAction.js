export const SET_LOGIN = "SET_LOGIN";

export const setToken = (data) => {
  return {
    type: SET_LOGIN,
    token: data,
  };
};
